# Video Manager

Contains the HTML and other necessary code for the video manager page. Mostly just images/links to the spee.ch videos.

Videos sourced from my LBRY channel at [https://spee.ch/@chirptheboy:6](https://spee.ch/@chirptheboy:6)

See it in action: [https://chirptheboy.gitlab.io/videos](https://chirptheboy.gitlab.io/videos)